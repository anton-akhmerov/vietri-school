{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Josephson junctions, proximity effect, and Majoranas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Important remark: the physics of Josephson junctions is a world of its own, we'll only consider a small fraction of it.\n",
    "\n",
    "Specifically we will consider $\\Delta$ fixed. If it was a dynamic variable, everything would become **much** harder.\n",
    "\n",
    "Still, there is an important difference between supercurrent and conductance: supercurrent is a property of all states, conductance is a Fermi surface property.\n",
    "\n",
    "$\\Rightarrow$ supercurrent is much more costly both to compute and interpret."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Role of junction size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from copy import copy\n",
    "\n",
    "import numpy as np\n",
    "from matplotlib import pyplot\n",
    "import kwant\n",
    "import kwant.continuum\n",
    "from pfaffian import pfaffian\n",
    "\n",
    "import ipywidgets\n",
    "\n",
    "pi = np.pi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def interact(function, **params):\n",
    "    params_spec = {\n",
    "        key: ipywidgets.FloatText(value=value, step=0.05)\n",
    "        for key, value in params.items()\n",
    "    }\n",
    "    return ipywidgets.interactive(function, **params_spec)\n",
    "\n",
    "def interval_shape(x_min, x_max):\n",
    "    def shape(site):\n",
    "        return x_min <= site.pos[0] < x_max\n",
    "    \n",
    "    return shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma_0 = np.eye(2)\n",
    "sigma_x = np.array(\n",
    "    [[0, 1],\n",
    "     [1, 0]]\n",
    ")\n",
    "sigma_y = np.array(\n",
    "    [[0, -1j],\n",
    "     [1j, 0]]\n",
    ")\n",
    "sigma_z = np.array(\n",
    "    [[1, 0],\n",
    "     [0, -1]]\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "majorana_hamiltonian = \"\"\"\n",
    "    (k_x**2 / 2 - mu(x)) * kron(sigma_0, sigma_z)\n",
    "    + alpha * k_x * kron(sigma_y, sigma_z)\n",
    "    + Delta_x(x) * kron(sigma_0, sigma_x)\n",
    "    + Delta_y(x) * kron(sigma_0, sigma_y)\n",
    "    + B_x * kron(sigma_x, sigma_0)\n",
    "\"\"\"\n",
    "\n",
    "phs = np.kron(sigma_y, sigma_y)\n",
    "\n",
    "template = kwant.continuum.discretize(majorana_hamiltonian)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_junction(\n",
    "    W_n,  # Width of the normal part including barriers\n",
    "    W_s,  # Width of the superconductors\n",
    "):\n",
    "    junction = kwant.Builder()\n",
    "    half_width = W_n // 2 + W_s\n",
    "    junction.fill(\n",
    "        template,\n",
    "        shape=interval_shape(-half_width, half_width),\n",
    "        start=[0]\n",
    "    )\n",
    "    return junction.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert raw parameters into position-dependent functions that we will use to define the system.\n",
    "\n",
    "def define_delta(Delta, phi, W_n):\n",
    "    def Delta_x(x):\n",
    "        if abs(x) <= W_n // 2:\n",
    "            return 0\n",
    "        elif x > 0:\n",
    "            return Delta * np.cos(phi)\n",
    "        else:\n",
    "            return Delta\n",
    "\n",
    "    def Delta_y(x):\n",
    "        if abs(x) <= W_n // 2:\n",
    "            return 0\n",
    "        elif x > 0:\n",
    "            return Delta * np.sin(phi)\n",
    "        else:\n",
    "            return 0\n",
    "    \n",
    "    return {'Delta_x': Delta_x, 'Delta_y': Delta_y}\n",
    "\n",
    "\n",
    "def define_mu(mu_n, mu_s, mu_barrier, W_n):\n",
    "    def mu(x):\n",
    "        if abs(x) < W_n // 2:\n",
    "            return mu_n\n",
    "        elif abs(x) == W_n // 2:\n",
    "            return mu_barrier\n",
    "        else:\n",
    "            return mu_s\n",
    "\n",
    "    return mu\n",
    "\n",
    "\n",
    "def convert_params(params):\n",
    "    \"\"\"Convert the raw parameters into position-dependent functions.\"\"\"\n",
    "    new = copy(params)\n",
    "    new['mu'] = define_mu(new.pop('mu_n'), new.pop('mu_s'), new.pop('mu_barrier'), new['W_n'])\n",
    "    Delta = define_delta(new.pop('Delta'), new.pop('phi'), new.pop('W_n'))\n",
    "    new.update(Delta)\n",
    "    return new"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to plot the spectrum of the junction (without Majoranas yet).\n",
    "\n",
    "We start with the smallest junction possible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geometry = dict(\n",
    "    W_n=0,\n",
    "    W_s=40,\n",
    ")\n",
    "\n",
    "junction = make_junction(**geometry)\n",
    "\n",
    "def current_vs_phase(\n",
    "    **params\n",
    "):\n",
    "    params.update(geometry)\n",
    "    params.update(B_x=0, alpha=0)\n",
    "    phis = np.linspace(0, 2*pi, 101)\n",
    "    Delta = params['Delta']\n",
    "    levels = []\n",
    "    for phi in phis:\n",
    "        params.update(phi=phi)\n",
    "        ham = junction.hamiltonian_submatrix(params=convert_params(params))\n",
    "        levels.append(np.linalg.eigvalsh(ham))\n",
    "    \n",
    "    pyplot.plot(phis, np.array(levels)/Delta)\n",
    "    pyplot.ylim(-3, 3)\n",
    "    pyplot.xlabel('ϕ')\n",
    "    pyplot.ylabel('E/Δ')\n",
    "\n",
    "interact(\n",
    "    current_vs_phase,\n",
    "    mu_n=.3,\n",
    "    mu_s=.3,\n",
    "    mu_barrier=.3,\n",
    "    Delta=0.05,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are in a **short junction** limit:\n",
    "- the number of normal levels within the gap $\\ll 1$\n",
    "- the junction is a $\\delta$-function scatterer\n",
    "- the wave function is mostly in the superconducting region"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When time-reversal symmetry is present, short junctions have\n",
    "\n",
    "$$\n",
    "E_n = \\pm \\Delta \\sqrt{1-T_n\\sin^2(\\phi/2)}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's turn to the opposite limit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geometry = dict(\n",
    "    W_n=20,\n",
    "    W_s=40,\n",
    ")\n",
    "\n",
    "junction = make_junction(**geometry)\n",
    "\n",
    "def current_vs_phase(\n",
    "    **params\n",
    "):\n",
    "    params.update(geometry)\n",
    "    params.update(B_x=0, alpha=0)\n",
    "    phis = np.linspace(0, 2*pi, 101)\n",
    "    Delta = params['Delta']\n",
    "    levels = []\n",
    "    for phi in phis:\n",
    "        params.update(phi=phi)\n",
    "        ham = junction.hamiltonian_submatrix(params=convert_params(params))\n",
    "        levels.append(np.linalg.eigvalsh(ham))\n",
    "    \n",
    "    pyplot.plot(phis, np.array(levels)/Delta)\n",
    "    pyplot.ylim(-3, 3)\n",
    "    pyplot.xlabel('ϕ')\n",
    "    pyplot.ylabel('E/Δ')\n",
    "\n",
    "interact(\n",
    "    current_vs_phase,\n",
    "    mu_n=.3,\n",
    "    mu_s=.3,\n",
    "    mu_barrier=.3,\n",
    "    Delta=.1,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are in the **long junction** limit; here:\n",
    "- If we remove the superconductor, the junction would have several bound states\n",
    "- The superconductor acts as a hard wall boundary\n",
    "- The wave function is mostly within the normal region"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Q: In what limit are the typical experimental devices?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A: Trick question: of course they are nearly always in-between the two!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Supercurrent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The total energy of the system is\n",
    "\n",
    "$$\n",
    "E_{gs} = \\frac{1}{2} \\sum_{E_n<0} E_n\n",
    "$$\n",
    "\n",
    "(The $1/2$ factor is tricky, and appears due to BdG equation)\n",
    "\n",
    "Supercurrent then is\n",
    "\n",
    "$$\n",
    "I(\\phi) = -\\frac{2e}{\\hbar}\\times\\frac{\\partial E_{gs}}{\\partial \\phi}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Easy in a short junction: we only need to know $S(E_E)$ (one can also generalize to TRS broken in the normal region)\n",
    "- Hard in a long junction: many levels contribute, including those above $\\Delta$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In tiny systems like our we can just use full diagonalization, in 3D or even 2D we would need to come up with more advanced tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geometry = dict(\n",
    "    W_n=20,\n",
    "    W_s=40,\n",
    ")\n",
    "\n",
    "junction = make_junction(**geometry)\n",
    "\n",
    "def current_vs_phase(\n",
    "    **params\n",
    "):\n",
    "    params.update(geometry)\n",
    "    params.update(B_x=0, alpha=0)\n",
    "    phis = np.linspace(0, 2*pi, 101)\n",
    "    Delta = params['Delta']\n",
    "    levels = []\n",
    "    for phi in phis:\n",
    "        params.update(phi=phi)\n",
    "        ham = junction.hamiltonian_submatrix(params=convert_params(params))\n",
    "        levels.append(np.linalg.eigvalsh(ham))\n",
    "\n",
    "    pyplot.figure(figsize=(9, 5))\n",
    "    pyplot.subplot(1, 2, 1)\n",
    "    pyplot.plot(phis, np.array(levels)/Delta)\n",
    "    pyplot.ylim(-3, 3)\n",
    "    pyplot.xlabel('ϕ')\n",
    "    pyplot.ylabel('E/Δ')\n",
    "\n",
    "    pyplot.subplot(1, 2, 2)\n",
    "    energy = np.sum(np.abs(np.array(levels)), axis=1) / 4\n",
    "    current = (np.roll(energy, 1) - energy) / (np.diff(phis)[0])\n",
    "    \n",
    "    pyplot.plot(phis, current)\n",
    "    pyplot.xlabel('ϕ')\n",
    "    pyplot.ylabel('$I\\hbar /2eΔ$')\n",
    "    pyplot.tight_layout()\n",
    "\n",
    "\n",
    "interact(\n",
    "    current_vs_phase,\n",
    "    mu_n=.3,\n",
    "    mu_s=.3,\n",
    "    mu_barrier=.3,\n",
    "    Delta=.1,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Majorana junctions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's turn on spin-orbit and magnetic field to get Majoranas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geometry = dict(\n",
    "    W_n=1,\n",
    "    W_s=40,\n",
    ")\n",
    "\n",
    "junction = make_junction(**geometry)\n",
    "\n",
    "def current_vs_phase(\n",
    "    **params\n",
    "):\n",
    "    params.update(geometry)\n",
    "    phis = np.linspace(0, 2*pi, 101)\n",
    "    Delta = params['Delta']\n",
    "    levels = []\n",
    "    for phi in phis:\n",
    "        params.update(phi=phi)\n",
    "        ham = junction.hamiltonian_submatrix(params=convert_params(params))\n",
    "        levels.append(np.linalg.eigvalsh(ham))\n",
    "\n",
    "    pyplot.figure(figsize=(9, 5))\n",
    "    pyplot.subplot(1, 2, 1)\n",
    "    pyplot.plot(phis, np.array(levels)/Delta)\n",
    "    pyplot.ylim(-3, 3)\n",
    "    pyplot.xlabel('ϕ')\n",
    "    pyplot.ylabel('E/Δ')\n",
    "\n",
    "    pyplot.subplot(1, 2, 2)\n",
    "    energy = np.sum(np.abs(np.array(levels)), axis=1) / 4\n",
    "    current = (np.roll(energy, 1) - energy) / (np.diff(phis)[0])\n",
    "    \n",
    "    pyplot.plot(phis, current)\n",
    "    pyplot.xlabel('ϕ')\n",
    "    pyplot.ylabel('$I\\hbar /2eΔ$')\n",
    "    pyplot.tight_layout()\n",
    "\n",
    "\n",
    "interact(\n",
    "    current_vs_phase,\n",
    "    mu_n=.3,\n",
    "    mu_s=.3,\n",
    "    mu_barrier=.3,\n",
    "    Delta=.1,\n",
    "    B_x=0,\n",
    "    alpha=0.2\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Two levels are from the decoupled Majoranas\n",
    "- Two other levels are due to the coupled Majoranas\n",
    "- The other two levels always have a protected crossing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dispersion relation in a Josephson junction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Revealing the biggest lie you've heard in this school: \"superconductor *proximitizes* a semiconductor\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geometry = dict(\n",
    "    W_n=20,\n",
    "    W_s=40,\n",
    ")\n",
    "\n",
    "junction = make_junction(**geometry)\n",
    "\n",
    "def current_vs_phase(\n",
    "    **params\n",
    "):\n",
    "    params.update(geometry)\n",
    "    params.update(B_x=0, alpha=0)\n",
    "    mus = np.linspace(params['mu_s'] - .2, params['mu_s'] + .2, 101)\n",
    "    Delta = params['Delta']\n",
    "    levels = []\n",
    "    for mu in mus:\n",
    "        params.update(mu_n=mu)\n",
    "        ham = junction.hamiltonian_submatrix(params=convert_params(params))\n",
    "        levels.append(np.linalg.eigvalsh(ham))\n",
    "    \n",
    "    pyplot.plot(mus, np.array(levels)/Delta)\n",
    "    pyplot.ylim(-3, 3)\n",
    "    pyplot.xlabel('μ')\n",
    "    pyplot.ylabel('E/Δ')\n",
    "\n",
    "interact(\n",
    "    current_vs_phase,\n",
    "    phi=0,\n",
    "    mu_s=.3,\n",
    "    mu_barrier=.3,\n",
    "    Delta=.1,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\Rightarrow$ the effective Hamiltonian we used for the Majorana junction is a very crude aproximation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- There are two types of junctions: long (hard to compute and analyse) and short (easier)\n",
    "- Supercurrent is carried by all states\n",
    "- Majoranas produce fractional Josephson current"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Zeeman field\n",
    "\n",
    "Remove spin-orbit coupling and observe how the spectrum changes with Zeeman field. Can you explain your observation?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Majoranas in a long junction\n",
    "\n",
    "Compute the current-phase relation with Majoranas in a long junction. Can you identify the contribution of Majoranas?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  },
  "toc-autonumbering": false
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Majorana signatures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We know that Majoranas appear in 1D nanowires with spin-orbit interaction and magnetic field:\n",
    "\n",
    "$$\n",
    "H = (p^2/2m - \\mu + \\alpha p \\sigma_y) \\tau_z + \\Delta(x) \\tau_x + B_x \\sigma_x\n",
    "$$\n",
    "\n",
    "It is topological when $B_x^2 > \\Delta^2 + \\mu^2$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a warm-up let us obtain its dispersion, discretize it, and verify that we have no surprises."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discretizing Majorana band structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from matplotlib import pyplot\n",
    "import kwant\n",
    "import kwant.continuum\n",
    "from pfaffian import pfaffian\n",
    "\n",
    "import ipywidgets\n",
    "\n",
    "pi = np.pi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def interact(function, **params):\n",
    "    params_spec = {\n",
    "        key: ipywidgets.FloatText(value=value, step=0.05)\n",
    "        for key, value in params.items()\n",
    "    }\n",
    "    return ipywidgets.interactive(function, **params_spec)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "majorana_hamiltonian = \"\"\"\n",
    "    (k_x**2 / 2 - mu) * kron(sigma_0, sigma_z)\n",
    "    + alpha * k_x * kron(sigma_y, sigma_z)\n",
    "    + Delta * kron(sigma_0, sigma_x)\n",
    "    + B_x * kron(sigma_x, sigma_0)\n",
    "\"\"\"\n",
    "\n",
    "h_cont = kwant.continuum.lambdify(majorana_hamiltonian)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "momenta = np.linspace(-4, 4, 100)\n",
    "\n",
    "def plot_spectrum(**params):\n",
    "    kwant.plotter.spectrum(h_cont, ('k_x', momenta), params=params)\n",
    "    \n",
    "interact(\n",
    "    plot_spectrum,\n",
    "    B_x=0,\n",
    "    mu=1,\n",
    "    alpha=0.5,\n",
    "    Delta=0\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wire_template = kwant.continuum.discretize(majorana_hamiltonian)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "infinite_wire = kwant.wraparound.wraparound(wire_template).finalized()\n",
    "\n",
    "def spectrum_discrete(**params):\n",
    "    kwant.plotter.spectrum(\n",
    "        infinite_wire,\n",
    "        ('k_x', np.linspace(-np.pi, np.pi, 301)),\n",
    "        params=params,\n",
    "    )\n",
    "\n",
    "interact(\n",
    "    spectrum_discrete,\n",
    "    B_x=0,\n",
    "    mu=0.3,\n",
    "    alpha=0.3,\n",
    "    Delta=0,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spectrum in a finite wire"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "finite_wire = kwant.Builder()\n",
    "\n",
    "def interval_shape(x_min, x_max):\n",
    "    def shape(site):\n",
    "        return x_min <= site.pos[0] < x_max\n",
    "    \n",
    "    return shape\n",
    "\n",
    "finite_wire.fill(wire_template, shape=interval_shape(0, 20), start=[10]);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def spectrum_finite(**params):\n",
    "    kwant.plotter.spectrum(\n",
    "        finite_wire.finalized(),\n",
    "        params=params,\n",
    "        x=('B_x', np.linspace(0, 1)),\n",
    "        show=False\n",
    "    )\n",
    "    pyplot.ylim(-.5, .5)\n",
    "\n",
    "interact(\n",
    "    spectrum_finite,\n",
    "    mu=0.3,\n",
    "    alpha=0.3,\n",
    "    Delta=0.3,    \n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We discovered \"[Majorana oscillations](https://www.google.com/search?q=majorana+oscillations+-neutrino)\"!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discrete symmetries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The BdG Hamiltonian may have a time-reversal symmetry\n",
    "\n",
    "$$\n",
    "\\mathcal{T} = \\sigma_y K;\\quad \\mathcal{T}^{-1}H(B, k_x)\\mathcal{T} = H(-B, -k_x),\n",
    "$$\n",
    "\n",
    "which only applies when $B=0$ (here $K$ is complex conjugation).\n",
    "\n",
    "It **always** has a particle-hole symmetry\n",
    "\n",
    "$$\n",
    "\\mathcal{P} = i \\mathcal{T} \\tau_y=i\\sigma_y \\tau_yK;\\quad \\mathcal{P}^{-1}H(k_x)\\mathcal{P} = -H(-k_x),\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma_0 = np.eye(2)\n",
    "sigma_x = np.array(\n",
    "    [[0, 1],\n",
    "     [1, 0]]\n",
    ")\n",
    "sigma_y = np.array(\n",
    "    [[0, -1j],\n",
    "     [1j, 0]]\n",
    ")\n",
    "sigma_z = np.array(\n",
    "    [[1, 0],\n",
    "     [0, -1]]\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Useful operators\n",
    "\n",
    "phs = np.kron(sigma_y, sigma_y)\n",
    "trs = np.kron(sigma_y, sigma_0)\n",
    "eh = -np.kron(sigma_0, sigma_z)  # Charge conservation, only there if Delta=0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "finite_wire.particle_hole = phs\n",
    "finite_wire.time_reversal = trs\n",
    "wire_finalized = finite_wire.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "default = dict(\n",
    "    B_x=0.04,\n",
    "    Delta=.3,\n",
    "    Delta_lead=0,\n",
    "    mu=.3,\n",
    "    mu_lead=.3,\n",
    "    alpha=.3,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sym = wire_finalized.discrete_symmetry()\n",
    "sym.validate(wire_finalized.hamiltonian_submatrix(\n",
    "    params={**default, 'B_x': 0}\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Andreev conductance\n",
    "\n",
    "**[Blackboard explanation]**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lead = kwant.Builder(\n",
    "    symmetry=kwant.TranslationalSymmetry([1]),\n",
    "    particle_hole=phs,\n",
    "    conservation_law=eh,\n",
    ")\n",
    "\n",
    "lead.fill(\n",
    "    wire_template.substituted(Delta='Delta_lead', mu='mu_lead'),\n",
    "    shape=(lambda site: True),\n",
    "    start=[0]\n",
    ")\n",
    "\n",
    "finite_wire.attach_lead(lead);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wire_finalized = finite_wire.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = kwant.smatrix(wire_finalized, params=default)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.round(s.data, 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r_ee = s.submatrix(lead_out=(0, 0), lead_in=(0, 0))\n",
    "np.round(r_ee, 3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "r_ee - s.submatrix((0, 1), (0, 1)).conj()[::-1, ::-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def andreev_conductance(s):\n",
    "    N_electrons = len(s.submatrix((0, 0), (0, 0)))\n",
    "    R_ee = s.transmission((0, 0), (0, 0))\n",
    "    R_he = s.transmission((0, 1), (0, 0))\n",
    "    return N_electrons - R_ee + R_he\n",
    "\n",
    "def compute_conductance(syst, energies, params):\n",
    "    conductances = []\n",
    "    for energy in energies:\n",
    "        conductances.append(\n",
    "            andreev_conductance(kwant.smatrix(\n",
    "                syst,\n",
    "                energy,\n",
    "                params=params\n",
    "            ))\n",
    "        )\n",
    "    return conductances"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "energies = np.linspace(-.5, .5, 101)\n",
    "\n",
    "def plot_conductance(**params):\n",
    "    pyplot.plot(\n",
    "        energies,\n",
    "        compute_conductance(wire_finalized, energies, params={**default, **params})\n",
    "    )\n",
    "\n",
    "interact(\n",
    "    plot_conductance,\n",
    "    B_x=0.00,\n",
    "    Delta=.3,\n",
    "    mu=.3,\n",
    "    mu_lead=.3,\n",
    "    alpha=.3,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have a problem due to the finite size effects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wire with two leads"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make the topological part infinite by converting it into another lead.\n",
    "\n",
    "This way we don't need to care about the finite size effects, coupling to another Majorana, and reflection of electrons from the opposite end of the wire."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "barrier = kwant.Builder()\n",
    "\n",
    "barrier.fill(\n",
    "    wire_template.substituted(Delta='Delta_lead', mu='mu_barrier'),\n",
    "    shape=interval_shape(0, 1),\n",
    "    start=[0],\n",
    ")\n",
    "\n",
    "topological_lead = kwant.Builder(\n",
    "    symmetry=kwant.TranslationalSymmetry([-1]),\n",
    "    particle_hole=phs,\n",
    ")\n",
    "\n",
    "topological_lead.fill(\n",
    "    wire_template,\n",
    "    shape=interval_shape(-np.inf, np.inf),\n",
    "    start=[0],\n",
    ")\n",
    "\n",
    "barrier.attach_lead(lead)\n",
    "barrier.attach_lead(topological_lead)\n",
    "\n",
    "kwant.plot(barrier)\n",
    "barrier = barrier.finalized()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "energies = np.linspace(-.5, .5, 101)\n",
    "\n",
    "def plot_conductance_infinite(**params):\n",
    "    pyplot.plot(\n",
    "        energies,\n",
    "        compute_conductance(barrier, energies, params={**default, **params})\n",
    "    )\n",
    "\n",
    "interact(\n",
    "    plot_conductance_infinite,\n",
    "    B_x=0,\n",
    "    Delta=.1,\n",
    "    mu=.5,\n",
    "    mu_lead=.5,\n",
    "    mu_barrier=.5,\n",
    "    alpha=.1,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Topological invariant"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "infinite_wire.hamiltonian_submatrix(params={**default, 'k_x': 0.0}) @ phs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_and_compute_invariant(**params):\n",
    "    H_0 = infinite_wire.hamiltonian_submatrix(params={**params, 'k_x': 0}) @ phs\n",
    "\n",
    "    H_pi = infinite_wire.hamiltonian_submatrix(params={**params, 'k_x': pi}) @ phs\n",
    "\n",
    "    kwant.plotter.spectrum(infinite_wire, ('k_x', np.linspace(-pi, pi, 301)), params=params, show=False)\n",
    "    pyplot.title(f\"Q = {np.sign(pfaffian(H_0) * pfaffian(H_pi)).real}\")\n",
    "\n",
    "interact(\n",
    "    plot_and_compute_invariant,\n",
    "    B_x=0.05,\n",
    "    mu=0.3,\n",
    "    alpha=0.3,\n",
    "    Delta=0.1,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because the particle-hole symmetry also applies to the scattering matrix (as long as we choose a proper basis for the modes), $Q=\\det S$ is an alternative way to compute the topological invariant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.linalg.det(s.data) # We computed it a while ago using a finite system wi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_infinite = kwant.smatrix(barrier, params=dict(\n",
    "    # Irrelevant for topology\n",
    "    mu_lead=0.5,\n",
    "    mu_barrier=0.5,\n",
    "    alpha=0.1,\n",
    "    Delta_lead=0,\n",
    "    # Relevant for topology\n",
    "    Delta=0.1,\n",
    "    mu=0.3,\n",
    "    B_x=0.1\n",
    "))\n",
    "\n",
    "np.linalg.det(s_infinite.data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions\n",
    "\n",
    "- We made Majoranas, yay!\n",
    "- Conductance with superconductors is due to Andreev reflection\n",
    "- The scattering matrix is constrained by particle-hole symmetry, and allows to compute a topological invariant"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Warm-up\n",
    "\n",
    "Once again, run all the code for the start. Ensure that you can change the parameters into a trivial and the topological regime."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tilted field\n",
    "\n",
    "Introduce magnetic field pointing in the $y$- and $z$-directions.\n",
    "\n",
    "Verify that the Hamiltonian still preserves particle-hole symmetry.\n",
    "\n",
    "Compute the band structure of the topological region and observe that the band gap closes when $B_y$ is large enough.\n",
    "Explain your observations.\n",
    "\n",
    "Related work: [arXiv:1406.6695](https://arxiv.org/abs/1406.6695)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Finite temperature effects\n",
    "\n",
    "So far we considered differential conductance at zero temperature, given by scattering at a fixed energy $E=eV$.\n",
    "\n",
    "Let's generalize this result to a finite temperature \n",
    "\n",
    "$$\n",
    "G(V, T) = \\int\\limits_{-\\infty}^{\\infty} G(E, T=0) \\frac{d f(E - eV, T)}{dT} dE,\n",
    "$$\n",
    "with $f(E, T) = 1/(\\exp(E/T) + 1)$ the Fermi-Dirac distribution.\n",
    "\n",
    "Your task is to implement this expression and compute its effect on the Majorana conductance (in particular the zero bias peak.\n",
    "\n",
    "1. Compute $G(E, T=0)$ for a range of energies first (just like we do above).\n",
    "2. Convolve the result with a derivative of the Fermi function as you vary temperature.\n",
    "3. Observe the effect a finite temperature has on conductance.\n",
    "\n",
    "NB: This is a minimal model of finite temperature effects, and it disregards a lot of important phenomena (e.g. inelastic scattering and reduction of the superconducting gap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Nonlocal transport\n",
    "\n",
    "Because $\\det r = +1$ in the trivial phase and $\\det r = -1$ in the topological, and because $\\det r$ is real due to particle-hole symmetry, there must be a perfectly transmitting mode at the transition point. We are going to study the appearance of this mode and the related charge transport.\n",
    "\n",
    "Here is your task:\n",
    "\n",
    "1. Define a scattering geometry with a finite superconducting wire and two normal leads attached.\n",
    "2. Compute the total transmission from left to right at $E=0$ as you drive the wire through the phase transition. Plot it together with $\\det r$.\n",
    "3. Compute the nonlocal conductance $G_{nonloc} = t_{ee} - t_{eh}$. Plot it as a function of $E$ and $B$ as you drive the system through the phase transition.\n",
    "\n",
    "Related works: [arXiv:1009.5542](https://arxiv.org/abs/1009.5542), [arXiv:1706.08888](https://arxiv.org/abs/1706.08888)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Quasi-Majorana states\n",
    "\n",
    "When $\\mu$ changes smoothly and not abruptly, and when the spin-orbit coupling $\\alpha$ is sufficiently small, [arXiv:1207.3067](https://arxiv.org/abs/1207.3067) predicted that bound states with an exponentially small energy appear, like a pair of Majorana states exactly on top of one another.\n",
    "\n",
    "Make $\\mu$ position-dependent, and make it smoothly vanish near one end of the nanowire. Make $\\mu$ large in the bulk of the wire, and observe appearance of quasi-Majorana states."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  },
  "toc-autonumbering": false
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
